---
author: Benoît Launay (sui generis)
title: Banyuls-sur-mer
app: editoria
book format:  7in × 10in
collection: pagedjs × editoria 2022
book orientation: portrait
sources: https://gitlab.coko.foundation/pagedjs/pagedjs-templates/theme-banyuls
---

# Theme name: Banyuls-sur-mer

### Typefaces

### Alfa slab one

An heavy weight font by [José Solé](https://www.jmsole.cl/) ([specimen on Google Fonts](https://fonts.google.com/specimen/Alfa+Slab+One)) used for the part and chapters opening title.

### Andana Pro

This type from the [Huerta Tipográphica type foundry](https://www.huertatipografica.com/en) is used for the main content (body and headlines) in three weights, each having its own italic variant. [The project repository](https://github.com/huertatipografica/Andada-Pro) is hosted on GitHub.

### Liberation Mono

The mono spaced variant from the _Liberation font family_ by Red Hat, Inc. has been chosen in case of need for ```pre``` blocks or ```code``` elements. [The project repository](https://github.com/liberationfonts/liberation-fonts) is hosted on GitHub.

## References

[Banyuls-sur-mer](https://en.wikipedia.org/wiki/Banyuls-sur-Mer) is a seaside station located on the southest arc of the French Mediterranean coast. It also the birth town of [Aristide Maillol](https://en.wikipedia.org/wiki/Aristide_Maillol), a sculptor and painter from the late 19<sup>th</sup> century until the first half of the 20<sup>th</sup> century.